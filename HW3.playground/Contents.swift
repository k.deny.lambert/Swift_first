import UIKit

// Задание 1

func fibonacciNumber(count: Int) -> [Int] {
    guard count > 0 else {return []} // Поменяла тут на срого больше 0
    
    var storyPointsNumbers = [Int]()
    storyPointsNumbers = [1] // Изменила условия, чтобы при count = 1 выводилось только 1 число
    
    var number1 = 0 // тут
    var nubmer2 = 1
    
    for _ in 0 ..< count - 1 { // и тут
        let number = number1 + nubmer2
        storyPointsNumbers.append(number)
        number1 = nubmer2
        nubmer2 = number
    }
    
    return storyPointsNumbers
}

print(fibonacciNumber(count: 5))
print(fibonacciNumber(count: 7))


// Задание 2

let printArrayFor = {() in
    for i in fibonacciNumber(count: 1) {
        print(i)
    }
}

printArrayFor()

print("Задание 2 - доработка") //Чуть дописала, чтобы можно было указывать число не в самом клоужере, а при его вызове

let printArrayFor2 = {(number: Int) -> Void in
    for i in fibonacciNumber(count: number) {
        print(i)
    }
}

printArrayFor2(5)
