import UIKit

// Задание 1

extension Int {
    func sumOfDigits() -> Int {
        var sum = 0
        var number = self
        
        if number < 0 {
            number = abs(number)
        }
        
        while number > 0 {
            sum += (number % 10)
            number = number / 10
        }
            return sum
    }
}

123.sumOfDigits()
(-731).sumOfDigits()
0.sumOfDigits()


// Задание 2

extension String {
    var numberFromText: Int {
        if Int(self) != nil {
            return Int(self)!
        } else {
            return 0
        }
    }
}

"104".numberFromText
"-53".numberFromText
"Text".numberFromText


// Задание 3

protocol СalculationFunction {
    func calculate () -> Double
}

class Multiplication: СalculationFunction {
    let number1: Double
    let number2: Double
    
    init (numberOne: Double, numberTwo: Double) {
        self.number1 = numberOne
        self.number2 = numberTwo
    }
    
    func calculate() -> Double {
        return number1 * number2
    }
    
}

class Division: СalculationFunction {
    let number1: Double
    let number2: Double
    
    init (numberOne: Double, numberTwo: Double) {
        self.number1 = numberOne
        self.number2 = numberTwo
    }
    
    func calculate() -> Double {
        return number1 / number2
    }
}

class SimpleActionCalculator {
    func calculate (_ operation: СalculationFunction) -> Double {
        return operation.calculate()
    }
}

var calculatior = SimpleActionCalculator()
let divisionСheck = calculatior.calculate(Division(numberOne: 10, numberTwo: 3))
let multiplicationCheck = calculatior.calculate(Multiplication(numberOne: 10, numberTwo: 3))
print(String(format: "%.2f", divisionСheck))

