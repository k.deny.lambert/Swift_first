import UIKit

// Задание 1

var evenArray = [Int]()

for number in 0...50 {
    if number % 2 == 0 {
        evenArray.append(number)
        print(number)
    }
}
print(evenArray)

var index = 0
while index < 10 {
    print(evenArray[index])
    index += 1
}

// Задание 2

enum referees: String {
    case vitalyVitalievich
    case olegOlegovich
    case michaelMikhailovich
}

var rating = [String:[referees:Double]] ()

rating["Chernyshov"] = [.vitalyVitalievich: 7, .olegOlegovich: 9, .michaelMikhailovich: 5]
rating["Belov"] = [.vitalyVitalievich: 3, .olegOlegovich: 5, .michaelMikhailovich: 6]

for (_, result) in rating {
    var sum: Double = 0
    for (_, resultVolue) in result {
        sum += resultVolue
    }
    let averageScore = sum / Double(result.count)
    print(String(format: "%.2f", averageScore))
}

