import UIKit

// Класс, претерпевший изменения в течение первых 3 заданий:

class UserInformation {
    
    let fieldHeader: String
    let fieldLength: Int
    var fieldPlaceholder: String?
    let fieldCode: Int?
    let fieldPriority: String
    
    init (fieldHeader: String, fieldLength: Int, fieldPlaceholder: String? = nil, fieldCode: Int? = nil, fieldPriority: String) {
        self.fieldHeader = fieldHeader
        self.fieldLength = fieldLength
        self.fieldPlaceholder = fieldPlaceholder
        self.fieldCode = fieldCode
        self.fieldPriority = fieldPriority
    }
    
    func characterLimit() -> Bool {
        return self.fieldLength >= 25
    }
}


// Задание 1

let userName = UserInformation(fieldHeader: "Name", fieldLength: 25, fieldPlaceholder: "Type your name", fieldCode: 1, fieldPriority: "high")
print(userName.fieldHeader)

let userSurname = UserInformation(fieldHeader: "Surname", fieldLength: 25, fieldPlaceholder: "Type your surname", fieldCode: 2, fieldPriority: "high")
print(userSurname.fieldLength)

let userAge = UserInformation(fieldHeader: "Age", fieldLength: 3, fieldPlaceholder: "Type your Age", fieldCode: 3, fieldPriority: "middle")
print(userAge.fieldPriority)

let userCity = UserInformation(fieldHeader: "City", fieldLength: 15, fieldPlaceholder: "Type your City", fieldCode: 4, fieldPriority: "low")
if let fieldCode = userCity.fieldCode { print(fieldCode) }


// Задание 2

let userCountry = UserInformation(fieldHeader: "Country", fieldLength: 26, fieldPlaceholder: "Type your Country", fieldCode: 5, fieldPriority: "low")
userCountry.characterLimit() // Проверка при привышении лимита

if let fieldPlaceholder = userCountry.fieldPlaceholder { print(fieldPlaceholder) }
userCountry.fieldPlaceholder = "Введите вашу страну" // Я просто сделала из константы fieldPlaceholder переменную, не знаю оптимальный ли это способ, но кажется все работает и без создания функции
if let fieldPlaceholder = userCountry.fieldPlaceholder { print(fieldPlaceholder) }

let userDistrict = UserInformation(fieldHeader: "District", fieldLength: 25, fieldPlaceholder: "Type your District", fieldCode: 6, fieldPriority: "low")
userDistrict.characterLimit() // Проверка при соответствии лимиту


// Задание 3 - Не поняла как сделать так, чтобы при создании экземпляра класса моно было не указывать опциональные параметры - если не прописываю nil для таких полей - получаю ошибку

let userRegion = UserInformation(fieldHeader: "Region", fieldLength: 26, fieldPriority: "low")
if let fieldCode = userRegion.fieldCode { print(fieldCode) }
if let fieldPlaceholder = userRegion.fieldPlaceholder { print(fieldPlaceholder) }

// Задание 4 - Для удобства скопировала и упростила класс

enum Priority: String {
    case high
    case middle
    case low = "lowww"
}

class UserInformation2 {
    
    let fieldHeader: String
    var fieldPriority: String
    let modePriority: Priority
    
    init (fieldHeader: String, modePriority: Priority) {
        self.fieldHeader = fieldHeader
        self.modePriority = modePriority
        self.fieldPriority = modePriority.rawValue
    }
}

let priorityMod = UserInformation2(fieldHeader: "houseNumber", modePriority: .low)
print(priorityMod.fieldPriority)

// Задание 5

struct UserInformationStruct {
    
    let fieldHeader: String
    let fieldLength: Int
    var fieldPlaceholder: String?
    let fieldCode: Int?
    
    func characterLimit() -> Bool {
        return self.fieldLength >= 25
    }
}

let userAddress = UserInformationStruct(fieldHeader: "Address", fieldLength: 25, fieldPlaceholder: "Type your Address", fieldCode: 5)
print(userAddress.fieldHeader)
if let fieldPlaceholder = userAddress.fieldPlaceholder { print(fieldPlaceholder) }
userAddress.characterLimit()
