import UIKit

//Задание 1

class Shape {
    func calculateArea() -> Double {
        fatalError ("not implemented")
    }
    func calculatePerimetr() -> Double {
        fatalError ("not implemented")
    }
}

//let baseShape = Shape()
//print(baseShape.calculateArea()) // Тут я проверяла как отрабатывает ошибка

// Прямоугольник
class Rectangle: Shape {
    private var length: Double
    private var width: Double
    
    init(length: Double, width: Double) {
        self.length = length
        self.width = width
    }
    
    override func calculateArea() -> Double {
        return length * width
    }
    override func calculatePerimetr() -> Double {
        return (length + width) * 2
    }
}

let rectSize = Rectangle(length: 3, width: 10)
print("Площадь прямоугольника =", rectSize.calculateArea()) // Просто проверка подсчетов
print("Периметр прямоугольника =", rectSize.calculatePerimetr())

// Квадрат
class Square: Shape {
    private var side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        return side * side
    }
    override func calculatePerimetr() -> Double {
        return side * 4
    }
}

let squareSize = Square(side: 5)
print("Площадь квадрата =", squareSize.calculateArea())
print("Периметр квадрата =", squareSize.calculatePerimetr())

// Круг
class Circle: Shape {
    private var radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        return radius * radius * Double.pi
    }
    override func calculatePerimetr() -> Double {
        return radius * 2 * Double.pi
    }
}

let circleSize = Circle(radius: 5)
print("Площадь круга =", circleSize.calculateArea())
print("Периметр круга =", circleSize.calculatePerimetr())


// Массив
let rectangleForArray = Rectangle(length: 6, width: 4)
let squareForArray = Square(side: 10)
let circleForArray = Circle(radius: 5)

let shapes = [rectangleForArray, squareForArray, circleForArray]

var area: Double = 0
var perimeter: Double = 0
        
for shape in shapes {
    area += shape.calculateArea()
    perimeter += shape.calculatePerimetr()
}

print("Total area =", String(format: "%.2f", area))
print("Total perimeter =", String(format: "%.2f", perimeter))


//Задание 2

func findIndexWithGenerics <T: Comparable> (search valueToFind: T, in array: [T]) -> Int? {

    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}
    

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]

if let findIndexWithGenerics = findIndexWithGenerics (search: "черепаха", in: arrayOfString) {
    print("Индекс: \(findIndexWithGenerics + 1)")
} else {
    print("Ничего не найдено по данному запросу")
}

let arrayOfNumbers = [5, 14, 4, 1, 3]

if let findIndexWithGenerics = findIndexWithGenerics (search: 15, in: arrayOfNumbers) {
    print("Индекс: \(findIndexWithGenerics + 1)")
} else {
    print("Ничего не найдено по данному запросу")
}

let arrayOfDouble = [4.5, 13.77, 3.14]
if let findIndexWithGenerics = findIndexWithGenerics (search: 3.14, in: arrayOfDouble) {
    print("Индекс: \(findIndexWithGenerics + 1)")
} else {
    print("Ничего не найдено по данному запросу")
}

