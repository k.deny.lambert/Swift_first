import UIKit

//Задача 1
var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

//Задача 2
let milkPrice: Int = 3

//Задача 3
var newMilkPrice = 4.20

//Задача 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0

if let milkCount = milkBottleCount {
    profit = newMilkPrice * Double(milkCount)
}
print(profit)

//Не использовала принудительное разворачивание milkBottleCount!, так как это может привести к ошибкам и даже крашу (в случае пустого значения опционала milkBottleCount)

// Еще пример разворачивания опционала:
var enterYourName: String?

if let sayMyName = enterYourName {
    print("- Say my name!" + " - You are \(sayMyName)")
} else {
    print("I don't know your name yet")
}

//Задача 5

var employeesList = [String]()
employeesList + ["Иван", "Петр", "Геннадий", "Марфа", "Андрей"]

//Задача 6

var isEveryoneWorkHard: Bool = false
var workingHours = 66
if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false //Вообще лишнее условие, так как в начале мы уже указали значение isEveryoneWorkHard
}

print(isEveryoneWorkHard)

